import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:help_pet_find_home/pet_detail_dialog.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Desgin/color.dart';
import 'animal_datas.dart';
import 'animal_model.dart';
import 'model/filter.dart';

class MyFavoritePage extends StatefulWidget {
  Filter filter;
  Future<List<AnimalData>> futureDatas;

  MyFavoritePage(this.futureDatas, this.filter);

  @override
  _MyFavoritePageState createState() => _MyFavoritePageState();
}

class _MyFavoritePageState extends State<MyFavoritePage> {
  List<String> favoriteList;
  SharedPreferences prefs;
  AnimalModel animalModel = AnimalModel();
  Future<List<AnimalData>> futureDatas;

  String getImageURL(String url) {
    if (url == '') {
      return null;
    } else {
      return url;
    }
  }

  void _openDialog(AnimalData animalData) {
    Navigator.of(context).push(MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return PetIntroDialog(animalData);
      },
      fullscreenDialog: true,
    ));
  }

  Widget getChip(String result) {
    if (result != null) {
      return Padding(
          padding: EdgeInsets.only(left: 8), child: Chip(label: Text(result)));
    } else {
      return Container();
    }
  }

  Widget getFavoriteIcon(int animalId) {
    if (favoriteList.contains(animalId.toString())) {
      return Icon(
        Icons.favorite,
        color: Colors.red[300],
      );
    } else {
      return Icon(
        Icons.favorite_border,
        color: Colors.black45,
      );
    }
  }

  saveFavorite(int animalId) {
    setState(() {
      bool isContainID = favoriteList.contains(animalId.toString());
      if (!isContainID) {
        favoriteList.add(animalId.toString());
        prefs.setStringList('Favorite_list', favoriteList);
      } else {
        favoriteList.remove(animalId.toString());
        prefs.setStringList('Favorite_list', favoriteList);
      }
    });
  }

  Future<List<AnimalData>> getFavorite() async {
    List<String> stringList = List<String>();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getStringList('Favorite_list') == null) {
      prefs.setStringList('Favorite_list', List<String>());
      stringList = prefs.getStringList('Favorite_list');
    } else {
      stringList = prefs.getStringList('Favorite_list');
    }

    List<int> favoriteList = stringList.map(int.parse).toList();
//    List<int> tempList = [11111, 22222, 3333];  for test
    widget.filter.animal_id = favoriteList;
    return widget.filter.startFavoriteFutureFilter();
  }

  Future<void> initSharePreferences() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.getStringList('Favorite_list') == null) {
      prefs.setStringList('Favorite_list', List<String>());
      favoriteList = prefs.getStringList('Favorite_list');
    } else {
      favoriteList = prefs.getStringList('Favorite_list');
    }
  }

  @override
  void initState() {
    initSharePreferences();
    initFutureDatas();

    super.initState();
  }

  initFutureDatas() {
    try {
      widget.futureDatas = getFavorite();
    } catch (e) {}
  }

  getSkelotnView() {
    return ListView.builder(
        padding: EdgeInsets.fromLTRB(8.0, 32.0, 8.0, 32.0),
        itemCount: 8,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            margin: EdgeInsets.all(8),
            child: InkWell(
              onTap: () {},
              child: Column(
                children: <Widget>[
                  Container(
                    height: 280,
                    width: double.infinity,
                    color: Colors.black26,
                    child: Center(
                      child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Colors.black26),
                      ),
                    ),
                  ),
                  ListTile(
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(4.0),
                          child: Container(
                            color: Colors.black26,
                            width: 80,
                            height: 24,
                          ),
                        ),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(4.0),
                          child: Container(
                            color: Colors.black26,
                            width: 80,
                            height: 24,
                          ),
                        ),
                      ],
                    ),
                    trailing: ClipRRect(
                      borderRadius: BorderRadius.circular(4.0),
                      child: Container(
                        color: Colors.black26,
                        width: 40,
                        height: 24,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: widget.futureDatas,
      builder:
          (BuildContext context, AsyncSnapshot<List<AnimalData>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
            return getSkelotnView();
          case ConnectionState.active:
          case ConnectionState.waiting:
            return getSkelotnView();
          case ConnectionState.done:
            if (snapshot.hasError) {
              return getSkelotnView();
            }
            if (snapshot.data.length == 0) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'images/tall_dog.png',
                    width: 100,
                    height: 200,
                  ),
                  Text(
                    '目前最愛沒有加入的毛小孩',
                    style: TextStyle(color: Colors.black54, fontSize: 22),
                  )
                ],
              );
            }
            return ListView.builder(
                padding: EdgeInsets.fromLTRB(8.0, 32.0, 8.0, 32.0),
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return ClipRRect(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(4),
                        topLeft: Radius.circular(4)),
                    child: Card(
                      margin: EdgeInsets.all(8),
                      child: InkWell(
                        onTap: () {
                          _openDialog(snapshot.data[index]);
                        },
                        child: Column(
                          children: <Widget>[
                            CachedNetworkImage(
                              imageUrl: snapshot.data[index].album_file,
                              fit: BoxFit.fitWidth,
                              height: 280,
                              width: double.infinity,
                              placeholder: (context, url) => Container(
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.black26),
                                ),
                              ),
                              errorWidget: (context, url, error) => Container(
                                color: Colors.black54,
                                height: 140,
                                width: double.infinity,
                                child: Icon(
                                  Icons.pets,
                                  size: 60,
                                  color: Colors.black38,
                                ),
                              ),
                            ),
                            ExpansionTile(
                              title: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Padding(
                                    child: Text(
                                      animalModel.getLocale(snapshot
                                          .data[index].animal_area_pkid),
                                      style: TextStyle(
                                          color: Colors.black54, fontSize: 20),
                                    ),
                                    padding: EdgeInsets.only(left: 8),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      IconButton(
                                        icon: Icon(Icons.share,
                                            color: Colors.black45),
                                        onPressed: () {
                                          Share.share(snapshot
                                                  .data[index].shelter_name +
                                              ' ' +
                                              snapshot.data[index].album_file);
                                        },
                                      ),
                                      IconButton(
                                        icon: getFavoriteIcon(
                                            snapshot.data[index].animal_id),
                                        onPressed: () {
                                          saveFavorite(
                                              snapshot.data[index].animal_id);
                                        },
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              children: <Widget>[
                                ListTile(
                                  leading: Icon(Icons.local_offer,
                                      color: primaryColor),
                                  title: Container(
                                    child: Wrap(
                                      children: <Widget>[
                                        getChip(animalModel.getKind(
                                            snapshot.data[index].animal_kind)),
                                        getChip(animalModel.getSex(
                                            snapshot.data[index].animal_sex)),
                                        getChip(animalModel.getBody(snapshot
                                            .data[index].animal_bodytype)),
                                        getChip(animalModel.getColour(snapshot
                                            .data[index].animal_colour)),
                                        getChip(animalModel.getAge(
                                            snapshot.data[index].animal_age)),
                                      ],
                                    ),
                                  ),
                                ),
                                ListTile(
                                  leading: Icon(
                                    Icons.place,
                                    color: primaryColor,
                                  ),
                                  subtitle: Text(animalModel.getCurrentPlace(
                                      snapshot.data[index].animal_place)),
                                  title: Text(
                                    '所在地',
                                    style: TextStyle(fontSize: 15),
                                  ),
                                ),
                                ListTile(
                                  leading:
                                      Icon(Icons.update, color: primaryColor),
                                  subtitle: Text(snapshot.data[index].cDate),
                                  title: Text('資料更新時間'),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                });
        }
        return null;
      },
    );
  }
}
