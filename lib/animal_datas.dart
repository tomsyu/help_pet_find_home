class AnimalDatas {
  final List<AnimalData> animalDatas;

  AnimalDatas({
    this.animalDatas,
  });

  factory AnimalDatas.fromJson(List<dynamic> json) {
    List<AnimalData> datas = json.map((i) => AnimalData.fromJson(i)).toList();

//    print(list.runtimeType);
//    List<AnimalData> datas =
//        list.map((i) => AnimalData.fromJson(i)).toList(); //see document

    return AnimalDatas(animalDatas: datas);
  }
}

class AnimalData {
  final int animal_id;
  final String animal_subid;
  final int animal_area_pkid;
  final int animal_shelter_pkid;
  final String animal_place;
  final String animal_kind;
  final String animal_sex;
  final String animal_bodytype;
  final String animal_colour;
  final String animal_age;
  final String animal_sterilization;
  final String animal_bacterin;
  final String animal_title;
  final String animal_status;
  final String animal_remark;
  final String animal_caption;
  final String animal_opendate;
  final String animal_closeddate;

  final String animal_update;
  final String animal_createtime;
  final String shelter_name;
  final String album_file;
  final String album_update;
  final String cDate;
  final String shelter_address;
  final String shelter_tel;
  final String animal_foundplace;

  AnimalData({
    this.animal_id,
    this.animal_subid,
    this.animal_area_pkid,
    this.animal_shelter_pkid,
    this.animal_place,
    this.animal_kind,
    this.animal_sex,
    this.animal_bodytype,
    this.animal_colour,
    this.animal_age,
    this.animal_sterilization,
    this.animal_bacterin,
    this.animal_title,
    this.animal_status,
    this.animal_remark,
    this.animal_caption,
    this.animal_opendate,
    this.animal_closeddate,
    this.animal_update,
    this.animal_createtime,
    this.shelter_name,
    this.album_file,
    this.album_update,
    this.cDate,
    this.shelter_address,
    this.shelter_tel,
    this.animal_foundplace,
  });

  factory AnimalData.fromJson(Map<String, dynamic> json) {
    return AnimalData(
      animal_id: json['animal_id'],
      animal_subid: json['animal_subid'],
      animal_area_pkid: json['animal_area_pkid'],
      animal_shelter_pkid: json['animal_shelter_pkid'],
      animal_place: json['animal_place'],
      animal_kind: json['animal_kind'],
      animal_sex: json['animal_sex'],
      animal_bodytype: json['animal_bodytype'],
      animal_colour: json['animal_colour'],
      animal_age: json['animal_age'],
      animal_sterilization: json['animal_sterilization'],
      animal_bacterin: json['animal_bacterin'],
      animal_title: json['animal_title'],
      animal_status: json['animal_status'],
      animal_remark: json['animal_remark'],
      animal_caption: json['animal_caption'],
      animal_opendate: json['animal_opendate'],
      animal_closeddate: json['animal_closeddate'],
      animal_update: json['animal_update'],
      animal_createtime: json['animal_createtime'],
      shelter_name: json['shelter_name'],
      album_file: json['album_file'],
      album_update: json['album_update'],
      cDate: json['cDate'],
      shelter_address: json['shelter_address'],
      shelter_tel: json['shelter_tel'],
      animal_foundplace: json['animal_foundplace'],
    );
  }
}
