import 'package:flutter/cupertino.dart';
import 'package:help_pet_find_home/model/filter_list.dart';
import 'package:help_pet_find_home/model/filter_state.dart';

import 'filter.dart';

class AppState extends ChangeNotifier {
  FilterList _filterList = FilterList();
  FilterState _filterState = FilterState();

  FilterList get filterList => _filterList;
  Filter _filter;

  Filter get filter => _filter;

  set filter(Filter value) {
    _filter = value;
  }

  set filterList(FilterList value) {
    _filterList = value;
  }

  FilterState get filterState => _filterState;

  void clearAll() {
    for (var key in filterState.animal_sex.keys) {
      filterState.animal_sex[key] = false;
    }
    filterList.animal_sex.clear();

    for (var key in filterState.animal_kind.keys) {
      filterState.animal_kind[key] = false;
    }
    filterList.animal_kind.clear();

    for (var key in filterState.animal_bodytype.keys) {
      filterState.animal_bodytype[key] = false;
    }
    filterList.animal_bodytype.clear();

    for (var key in filterState.animal_age.keys) {
      filterState.animal_age[key] = false;
    }
    filterList.animal_age.clear();

    for (var key in filterState.animal_sterilization.keys) {
      filterState.animal_sterilization[key] = false;
    }
    filterList.animal_sterilization.clear();

    for (var key in filterState.animal_area_pkid.keys) {
      filterState.animal_area_pkid[key] = false;
    }
    filterList.animal_area_pkid.clear();

    filterState.animal_shelter_pkid[0] = -1;

    filterList.animal_shelter_pkid.clear();
  }

  bool isFilterListHasChild() {
    if (_filterList.animal_kind.isEmpty &&
        _filterList.animal_sterilization.isEmpty &&
        _filterList.animal_sex.isEmpty &&
        _filterList.animal_area_pkid.isEmpty &&
        _filterList.animal_shelter_pkid.isEmpty &&
        _filterList.animal_bodytype.isEmpty &&
        _filterList.animal_age.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  bool isFilterStateHasChild() {
    if (_filterState.animal_kind.isEmpty &&
        _filterState.animal_sterilization.isEmpty &&
        _filterState.animal_sex.isEmpty &&
        _filterState.animal_area_pkid.isEmpty &&
        _filterState.animal_shelter_pkid.isEmpty &&
        _filterState.animal_bodytype.isEmpty &&
        _filterState.animal_age.isEmpty) {
      return false;
    } else {
      return true;
    }
  }

  void update() {
    notifyListeners();
  }
}
