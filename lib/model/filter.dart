import 'package:help_pet_find_home/animal_datas.dart';

class Filter {
  final List<AnimalData> animalData;

  Filter(this.animalData);
  List<String> _animal_sex;
  List<String> _animal_bodytypes;
  List<String> _animal_kind;
  List<String> _animal_age;
  List<String> _animal_colour;
  List<String> _animal_sterilization;
  List<String> _animal_bacterin;
  List<int> _animal_area_pkid;
  List<int> _animal_id;
  List<int> _animal_shelter_pkid;

  List<String> get animal_sex => _animal_sex;

  set animal_sex(List<String> value) {
    _animal_sex = value;
  } //  int animal_id;

  bool filterAnimalBodytypes(AnimalData item) {
    if (_animal_bodytypes.length == 0) {
      return true;
    }

    for (String bodytype in _animal_bodytypes) {
      if (item.animal_bodytype == bodytype) {
        return true;
      }
    }
    return false;
  }

  bool filterAnimalSex(AnimalData item) {
    if (_animal_sex.length == 0) {
      return true;
    }
    for (String sex in animal_sex) {
      if (item.animal_sex == sex) {
        return true;
      }
    }
    return false;
  }

  bool filterAnimalKind(AnimalData item) {
    if (_animal_kind.length == 0) {
      return true;
    }

    for (String kind in _animal_kind) {
      if (item.animal_kind == kind) {
        return true;
      } else if (item.animal_kind != '貓' && item.animal_kind != '狗') {
        return true;
      }
    }
    return false;
  }

  bool filterAnimalAge(AnimalData item) {
    if (_animal_age.length == 0) {
      return true;
    }

    for (String age in _animal_age) {
      if (item.animal_age == age) {
        return true;
      }
    }
    return false;
  }

  bool filterAnimalColour(AnimalData item) {
    if (_animal_colour.length == 0) {
      return true;
    }

    for (String colour in _animal_colour) {
      if (item.animal_colour == colour) {
        return true;
      }
    }
    return false;
  }

  bool filterAnimalSterilization(AnimalData item) {
    if (_animal_sterilization.length == 0) {
      return true;
    }

    for (String sterilization in _animal_sterilization) {
      if (item.animal_sterilization == sterilization) {
        return true;
      }
    }
    return false;
  }

  bool filterAnimalBacterin(AnimalData item) {
    if (_animal_bacterin.length == 0) {
      return true;
    }

    for (String backterin in _animal_bacterin) {
      if (item.animal_bacterin == backterin) {
        return true;
      }
    }
    return false;
  }

  bool filterAnimalAreaPkID(AnimalData item) {
    if (_animal_area_pkid.length == 0) {
      return true;
    }

    for (int areaPkid in _animal_area_pkid) {
      if (item.animal_area_pkid == areaPkid) {
        return true;
      }
    }
    return false;
  }

  bool filterAnimalID(AnimalData item) {
    if (_animal_id.length == 0) {
      return false;
    }

    for (int animalid in _animal_id) {
      if (item.animal_id == animalid) {
        return true;
      }
    }
    return false;
  }

  bool filterShelterPkid(AnimalData item) {
    if (_animal_shelter_pkid.length == 0 || _animal_shelter_pkid[0] == -1) {
      return true;
    }

    for (int animalid in _animal_shelter_pkid) {
      if (item.animal_shelter_pkid == animalid) {
        return true;
      }
    }
    return false;
  }

  bool filterAlbumFile(AnimalData item) {
    if (_animal_bacterin.length == 0) {
      return true;
    }

    for (String backterin in _animal_bacterin) {
      if (item.animal_bacterin == backterin) {
        return true;
      }
    }
    return false;
  }

  Future<List<AnimalData>> startFutureFilter() async {
    List<AnimalData> results = List<AnimalData>();
    for (int i = 0; i < animalData.length; i++) {
      AnimalData item = animalData[i];
      if (filterAnimalSex(item) &&
          filterAnimalBodytypes(item) &&
          filterAnimalKind(item) &&
          filterAnimalAge(item) &&
          filterAnimalAreaPkID(item) &&
          filterShelterPkid(item) &&
          filterAnimalSterilization(item)) {
        results.add(animalData[i]);
      }
    }
    return results;
  }

  Future<List<AnimalData>> startFavoriteFutureFilter() async {
    List<AnimalData> results = List<AnimalData>();
    for (int i = 0; i < animalData.length; i++) {
      AnimalData item = animalData[i];
      if (filterAnimalID(item)) {
        results.add(animalData[i]);
      }
    }
    return results;
  }

  List<String> get animal_bodytypes => _animal_bodytypes;

  set animal_bodytypes(List<String> value) {
    _animal_bodytypes = value;
  }

  List<String> get animal_kind => _animal_kind;

  set animal_kind(List<String> value) {
    _animal_kind = value;
  }

  List<String> get animal_age => _animal_age;

  set animal_age(List<String> value) {
    _animal_age = value;
  }

  List<String> get animal_colour => _animal_colour;

  set animal_colour(List<String> value) {
    _animal_colour = value;
  }

  List<String> get animal_sterilization => _animal_sterilization;

  set animal_sterilization(List<String> value) {
    _animal_sterilization = value;
  }

  List<String> get animal_bacterin => _animal_bacterin;

  set animal_bacterin(List<String> value) {
    _animal_bacterin = value;
  }

  List<int> get animal_area_pkid => _animal_area_pkid;

  set animal_area_pkid(List<int> value) {
    _animal_area_pkid = value;
  }

  List<int> get animal_id => _animal_id;

  set animal_id(List<int> value) {
    _animal_id = value;
  }

  List<int> get animal_shelter_pkid => _animal_shelter_pkid;

  set animal_shelter_pkid(List<int> value) {
    _animal_shelter_pkid = value;
  }
}
