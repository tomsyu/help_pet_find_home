class FilterState {
  Map<String, bool> _animal_sex = Map<String, bool>();
  Map<String, bool> _animal_kind = Map<String, bool>();
  Map<String, bool> _animal_bodytype = Map<String, bool>();
  Map<String, bool> _animal_age = Map<String, bool>();

  Map<String, bool> _animal_sterilization = Map<String, bool>();
  Map<String, bool> _animal_bacterin = Map<String, bool>();
  Map<int, bool> _animal_area_pkid = Map<int, bool>();
  List<int> _animal_shelter_pkid = List<int>();

  FilterState() {
    _animal_sex['F'] = false;
    _animal_sex['M'] = false;

    _animal_kind['貓'] = false;
    _animal_kind['狗'] = false;
    _animal_kind['其他'] = false;

    _animal_bodytype['SMALL'] = false;
    _animal_bodytype['MEDIUM'] = false;
    _animal_bodytype['BIG'] = false;

    _animal_age['CHILD'] = false;
    _animal_age['ADULT'] = false;

    _animal_sterilization['T'] = false;
    _animal_sterilization['F'] = false;
    _animal_sterilization['N'] = false;

    _animal_bacterin['T'] = false;
    _animal_bacterin['F'] = false;
    _animal_bacterin['N'] = false;
    for (int i = 2; i < 24; i++) {
      _animal_area_pkid[i] = false;
    }
    _animal_shelter_pkid = [-1];
  }

  Map<String, bool> get animal_sex => _animal_sex;

  List<int> get animal_shelter_pkid => _animal_shelter_pkid;

  Map<String, bool> get animal_kind => _animal_kind;

  Map<int, bool> get animal_area_pkid => _animal_area_pkid;

  Map<String, bool> get animal_bacterin => _animal_bacterin;

  Map<String, bool> get animal_sterilization => _animal_sterilization;

  Map<String, bool> get animal_age => _animal_age;

  Map<String, bool> get animal_bodytype => _animal_bodytype;
}
