class FilterList {
  List<String> _animal_sex;
  List<String> _animal_bodytype;
  List<String> _animal_kind;
  List<String> _animal_colour;
  List<String> _animal_age;
  List<String> _animal_sterilization;
  List<String> _animal_bacterin;
  List<int> _animal_area_pkid;
  List<int> _animal_id;
  List<int> _animal_shelter_pkid;

  FilterList() {
    _animal_sex = List<String>();
    _animal_bodytype = List<String>();
    _animal_kind = List<String>();
    _animal_colour = List<String>();
    _animal_age = List<String>();
    _animal_sterilization = List<String>();
    _animal_bacterin = List<String>();
    _animal_area_pkid = List<int>();
    _animal_id = List<int>();
    _animal_shelter_pkid = List<int>();
  }

  List<String> get animal_bacterin => _animal_bacterin;

  set animal_bacterin(List<String> value) {
    _animal_bacterin = value;
  }

  List<String> get animal_sterilization => _animal_sterilization;

  set animal_sterilization(List<String> value) {
    _animal_sterilization = value;
  }

  List<String> get animal_age => _animal_age;

  set animal_age(List<String> value) {
    _animal_age = value;
  }

  List<String> get animal_colour => _animal_colour;

  set animal_colour(List<String> value) {
    _animal_colour = value;
  }

  List<String> get animal_kind => _animal_kind;

  set animal_kind(List<String> value) {
    _animal_kind = value;
  }

  List<String> get animal_bodytype => _animal_bodytype;

  set animal_bodytype(List<String> value) {
    _animal_bodytype = value;
  }

  List<String> get animal_sex => _animal_sex;

  set animal_sex(List<String> value) {
    _animal_sex = value;
  }

  List<int> get animal_area_pkid => _animal_area_pkid;

  set animal_area_pkid(List<int> value) {
    _animal_area_pkid = value;
  }

  List<int> get animal_id => _animal_id;

  set animal_id(List<int> value) {
    _animal_id = value;
  }

  List<int> get animal_shelter_pkid => _animal_shelter_pkid;

  set animal_shelter_pkid(List<int> value) {
    _animal_shelter_pkid = value;
  }
}
