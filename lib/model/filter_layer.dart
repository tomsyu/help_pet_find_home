import 'package:flutter/material.dart';
import 'package:help_pet_find_home/Desgin/color.dart';
import 'package:help_pet_find_home/animal_model.dart';
import 'package:help_pet_find_home/model/app_state.dart';
import 'package:provider/provider.dart';
import 'filter_list.dart';

class FilterLayer extends StatefulWidget {
  final ValueChanged<FilterList> onCategoryTap;
  final VoidCallback onSearchPress;

  FilterLayer({this.onCategoryTap, this.onSearchPress});

  @override
  _FilterLayerState createState() => _FilterLayerState();
}

class _FilterLayerState extends State<FilterLayer> {
  AnimalModel animalModel;
  List<int> areaPkidList;
  String dropdownValue;
  TextStyle titleStyle = TextStyle(color: Colors.white);

  @override
  void initState() {
    animalModel = AnimalModel();
    areaPkidList = List<int>.generate(22, (i) => i = i + 2);
//    print(animalModel.getShelterNameByPkID(48));

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AppState>(
      builder: (context, appState, child) {
        return Column(
          children: <Widget>[
            Container(
              color: primaryColor,
              child: Divider(
                indent: 8,
                endIndent: 8,
                color: primaryColorLight,
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Center(
                  child: Container(
                    padding: EdgeInsets.all(16),
                    color: primaryColor,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Text(
                          '選擇性別',
                          style: titleStyle,
                        ),
                        Wrap(
                          spacing: 8,
                          children: <Widget>[
                            MyFilterChip(
                              selected: appState.filterState.animal_sex['F'],
                              label: animalModel.getSex('F'),
                              onSelected: (bool) {
                                appState.filterState.animal_sex['F'] = bool;
                                if (bool) {
                                  appState.filterList.animal_sex.add('F');
                                } else {
                                  appState.filterList.animal_sex.remove('F');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                            MyFilterChip(
                              selected: appState.filterState.animal_sex['M'],
                              label: animalModel.getSex('M'),
                              onSelected: (bool) {
                                appState.filterState.animal_sex['M'] = bool;
                                if (bool) {
                                  appState.filterList.animal_sex.add('M');
                                } else {
                                  appState.filterList.animal_sex.remove('M');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                          ],
                        ),
                        MyDivider(),
                        Text('選擇類型', style: titleStyle),
                        Wrap(
                          spacing: 8,
                          children: <Widget>[
                            MyFilterChip(
                              selected: appState.filterState.animal_kind['貓'],
                              label: '貓',
                              onSelected: (bool) {
                                appState.filterState.animal_kind['貓'] = bool;
                                if (bool) {
                                  appState.filterList.animal_kind.add('貓');
                                } else {
                                  appState.filterList.animal_kind.remove('貓');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                            MyFilterChip(
                              selected: appState.filterState.animal_kind['狗'],
                              label: '狗',
                              onSelected: (bool) {
                                appState.filterState.animal_kind['狗'] = bool;
                                if (bool) {
                                  appState.filterList.animal_kind.add('狗');
                                } else {
                                  appState.filterList.animal_kind.remove('狗');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                            MyFilterChip(
                              selected: appState.filterState.animal_kind['其他'],
                              label: '其他',
                              onSelected: (bool) {
                                appState.filterState.animal_kind['其他'] = bool;
                                if (bool) {
                                  appState.filterList.animal_kind.add('其他');
                                } else {
                                  appState.filterList.animal_kind.remove('其他');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                          ],
                        ),
                        MyDivider(),
                        Text('選擇體型', style: titleStyle),
                        Wrap(
                          spacing: 8,
                          children: <Widget>[
                            MyFilterChip(
                              selected:
                                  appState.filterState.animal_bodytype['SMALL'],
                              label: '小',
                              onSelected: (bool) {
                                appState.filterState.animal_bodytype['SMALL'] =
                                    bool;
                                if (bool) {
                                  appState.filterList.animal_bodytype
                                      .add('SMALL');
                                } else {
                                  appState.filterList.animal_bodytype
                                      .remove('SMALL');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                            MyFilterChip(
                              selected: appState
                                  .filterState.animal_bodytype['MEDIUM'],
                              label: '中',
                              onSelected: (bool) {
                                appState.filterState.animal_bodytype['MEDIUM'] =
                                    bool;
                                if (bool) {
                                  appState.filterList.animal_bodytype
                                      .add('MEDIUM');
                                } else {
                                  appState.filterList.animal_bodytype
                                      .remove('MEDIUM');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                            MyFilterChip(
                              selected:
                                  appState.filterState.animal_bodytype['BIG'],
                              label: '大',
                              onSelected: (bool) {
                                appState.filterState.animal_bodytype['BIG'] =
                                    bool;
                                if (bool) {
                                  appState.filterList.animal_bodytype
                                      .add('BIG');
                                } else {
                                  appState.filterList.animal_bodytype
                                      .remove('BIG');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                          ],
                        ),
                        MyDivider(),
                        Text('選擇住所', style: titleStyle),
                        Theme(
                          data: Theme.of(context)
                              .copyWith(canvasColor: primaryColorDark),
                          child: Container(
                            width: 60,
                            child: DropdownButton<String>(
                              iconEnabledColor: Colors.white,
                              value: animalModel.getShelterNameByPkID(
                                  appState.filterState.animal_shelter_pkid[0]),
                              hint: Text('選擇收容所'),
                              icon: Icon(
                                Icons.keyboard_arrow_down,
                              ),
                              iconSize: 24,
                              style: TextStyle(color: Colors.white),
                              onChanged: (String newValue) {
                                setState(() {
                                  appState.filterState.animal_shelter_pkid[0] =
                                      animalModel
                                          .getShelterKeyByValue(newValue);

                                  int key = animalModel
                                      .getShelterKeyByValue(newValue);
                                  if (key != -1) {
                                    appState.filterList.animal_shelter_pkid
                                        .clear();
                                    appState.filterList.animal_shelter_pkid
                                        .add(key);
                                    appState.filterState.animal_shelter_pkid
                                        .clear();
                                    appState.filterState.animal_shelter_pkid
                                        .add(key);
                                  } else {
                                    appState.filterList.animal_shelter_pkid
                                        .clear();
                                    appState.filterState.animal_shelter_pkid
                                        .clear();
                                    appState.filterState.animal_shelter_pkid
                                        .add(key);
                                  }
                                });
                                widget.onCategoryTap(appState.filterList);
                              },
                              items: <String>[
                                animalModel.shelterMap[-1],
                                animalModel.shelterMap[48],
                                animalModel.shelterMap[49],
                                animalModel.shelterMap[50],
                                animalModel.shelterMap[51],
                                animalModel.shelterMap[53],
                                animalModel.shelterMap[55],
                                animalModel.shelterMap[56],
                                animalModel.shelterMap[58],
                                animalModel.shelterMap[59],
                                animalModel.shelterMap[60],
                                animalModel.shelterMap[61],
                                animalModel.shelterMap[62],
                                animalModel.shelterMap[63],
                                animalModel.shelterMap[67],
                                animalModel.shelterMap[68],
                                animalModel.shelterMap[69],
                                animalModel.shelterMap[70],
                                animalModel.shelterMap[71],
                                animalModel.shelterMap[72],
                                animalModel.shelterMap[73],
                                animalModel.shelterMap[74],
                                animalModel.shelterMap[75],
                                animalModel.shelterMap[76],
                                animalModel.shelterMap[77],
                                animalModel.shelterMap[78],
                                animalModel.shelterMap[79],
                                animalModel.shelterMap[80],
                                animalModel.shelterMap[81],
                                animalModel.shelterMap[82],
                                animalModel.shelterMap[83],
                                animalModel.shelterMap[89],
                                animalModel.shelterMap[92],
                                animalModel.shelterMap[96],
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          ),
// Your Dropdown Code Here,
                        ),
                        MyDivider(),
                        Text('所在縣市', style: titleStyle),
                        Wrap(
                          spacing: 16,
                          runSpacing: 8,
                          children: areaPkidList.map<Widget>((s) {
                            return MyFilterChip(
                              label: animalModel.getLocale(s),
                              selected:
                                  appState.filterState.animal_area_pkid[s],
                              onSelected: (bool) {
                                appState.filterState.animal_area_pkid[s] = bool;
                                if (bool) {
                                  appState.filterList.animal_area_pkid.add(s);
                                } else {
                                  appState.filterList.animal_area_pkid
                                      .remove(s);
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            );
                          }).toList(),
                        ),
                        MyDivider(),
                        Text('年紀', style: titleStyle),
                        Wrap(
                          spacing: 8,
                          children: <Widget>[
                            MyFilterChip(
                              selected:
                                  appState.filterState.animal_age['CHILD'],
                              label: '幼年',
                              onSelected: (bool) {
                                appState.filterState.animal_age['CHILD'] = bool;
                                if (bool) {
                                  appState.filterList.animal_age.add('CHILD');
                                } else {
                                  appState.filterList.animal_age
                                      .remove('CHILD');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                            MyFilterChip(
                              selected:
                                  appState.filterState.animal_age['ADULT'],
                              label: '成年',
                              onSelected: (bool) {
                                appState.filterState.animal_age['ADULT'] = bool;
                                if (bool) {
                                  appState.filterList.animal_age.add('ADULT');
                                } else {
                                  appState.filterList.animal_age
                                      .remove('ADULT');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                          ],
                        ),
                        MyDivider(),
                        Text(
                          '是否絕育',
                          style: titleStyle,
                        ),
                        Wrap(
                          spacing: 8,
                          children: <Widget>[
                            MyFilterChip(
                              selected: appState
                                  .filterState.animal_sterilization['T'],
                              label: '是',
                              onSelected: (bool) {
                                appState.filterState.animal_sterilization['T'] =
                                    bool;
                                if (bool) {
                                  appState.filterList.animal_sterilization
                                      .add('T');
                                } else {
                                  appState.filterList.animal_sterilization
                                      .remove('T');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                            MyFilterChip(
                              selected: appState
                                  .filterState.animal_sterilization['F'],
                              label: '否',
                              onSelected: (bool) {
                                appState.filterState.animal_sterilization['F'] =
                                    bool;
                                if (bool) {
                                  appState.filterList.animal_sterilization
                                      .add('F');
                                } else {
                                  appState.filterList.animal_sterilization
                                      .remove('F');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                            MyFilterChip(
                              selected: appState
                                  .filterState.animal_sterilization['N'],
                              label: '未知',
                              onSelected: (bool) {
                                appState.filterState.animal_sterilization['N'] =
                                    bool;
                                if (bool) {
                                  appState.filterList.animal_sterilization
                                      .add('N');
                                } else {
                                  appState.filterList.animal_sterilization
                                      .remove('N');
                                }
                                widget.onCategoryTap(appState.filterList);
                              },
                            ),
                          ],
                        ),
                        MyDivider(),
                        SizedBox(
                          height: 48,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
//            Container(
//              width: double.infinity,
//              padding: EdgeInsets.all(8),
//              color: primaryColor,
//              child: RaisedButton.icon(
//                icon: Icon(
//                  Icons.search,
//                  color: Colors.white,
//                ),
//                color: primaryColorLight,
//                label: Text(
//                  '搜尋',
//                  style: TextStyle(color: Colors.white, fontSize: 19),
//                ),
//                onPressed: () {
//                  widget.onCategoryTap(appState.filterList);
//                  appState.update();
//                  widget.onSearchPress();
//                },
//              ),
//            )
          ],
        );
      },
    );
  }
}

class MyDivider extends StatelessWidget {
  const MyDivider({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Divider(
      height: 32,
      color: primaryColorLight,
    );
  }
}

class MyFilterChip extends StatelessWidget {
  final bool selected;
  final String label;
  final ValueChanged<bool> onSelected;

  MyFilterChip({this.selected, this.label, this.onSelected});

  @override
  Widget build(BuildContext context) {
    return FilterChip(
      selected: selected,
      label: Text(
        label,
        style: TextStyle(color: Colors.white),
      ),
      onSelected: onSelected,
      backgroundColor: primaryColorLight,
      selectedColor: primaryColorDark,
      checkmarkColor: Colors.white,
    );
  }
}
