import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:help_pet_find_home/Desgin/color.dart';
import 'package:help_pet_find_home/animal_datas.dart';
import 'package:help_pet_find_home/pet_detail_dialog.dart';
import 'package:http/http.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'animal_model.dart';

class Adoption extends StatefulWidget {
  final Future<List<AnimalData>> futureDatas;
  Adoption(this.futureDatas);

  @override
  _AdoptionState createState() => _AdoptionState();
}

class _AdoptionState extends State<Adoption> {
  AnimalModel animalModel = AnimalModel();
  String url =
      'https://data.coa.gov.tw/Service/RedirectService.aspx?UnitId=QcbUEzN6E6DL&url=http%3a%2f%2fdata.coa.gov.tw%2fService%2fOpenData%2fTransService.aspx%3fUnitId%3dQcbUEzN6E6DL';
  List<String> favoriteList;
  SharedPreferences prefs;

  Future<AnimalDatas> getData() async {
    Response response = await get(url);
    final responseJson = await json.decode(response.body);

    return AnimalDatas.fromJson(responseJson);
  }

  String getImageURL(String url) {
    if (url == '') {
      return null;
    } else {
      return url;
    }
  }

  void _openDialog(AnimalData animalData) {
    Navigator.of(context).push(MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return PetIntroDialog(animalData);
      },
      fullscreenDialog: true,
    ));
  }

  Widget getChip(String result) {
    if (result != null) {
      return Padding(
        padding: EdgeInsets.only(left: 8),
        child: Chip(
          label: Text(result),
          labelStyle: TextStyle(
            color: Colors.black54,
          ),
        ),
      );
    } else {
      return Container(
        width: 0,
      );
    }
  }

  bool saveFavorite(int animalId) {
    setState(() {
      bool isContainID = favoriteList.contains(animalId.toString());
      if (!isContainID) {
        favoriteList.add(animalId.toString());
        prefs.setStringList('Favorite_list', favoriteList);
        return true;
      } else {
        favoriteList.remove(animalId.toString());
        prefs.setStringList('Favorite_list', favoriteList);
        return false;
      }
    });
  }

  Future<void> initSharePreferences() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.getStringList('Favorite_list') == null) {
      prefs.setStringList('Favorite_list', List<String>());
      favoriteList = prefs.getStringList('Favorite_list');
    } else {
      favoriteList = prefs.getStringList('Favorite_list');
    }
  }

  List<String> getFavoriteList() {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getStringList('Favorite_list') == null) {
      prefs.setStringList('Favorite_list', List<String>());
    }

    List<String> favoriteList = prefs.getStringList('Favorite_list');
    return favoriteList;
  }

  Widget getFavoriteIcon(int animalId) {
    if (favoriteList == null) {
      return Icon(
        Icons.favorite_border,
        color: Colors.black45,
      );
    }
    if (favoriteList.contains(animalId.toString())) {
      return Icon(
        Icons.favorite,
        color: Colors.red[300],
      );
    } else {
      return Icon(
        Icons.favorite_border,
        color: Colors.black45,
      );
    }
  }

  @override
  void initState() {
    initSharePreferences();
//    favoriteList = getFavoriteList();
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: widget.futureDatas,
      builder:
          (BuildContext context, AsyncSnapshot<List<AnimalData>> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
            return ListView.builder(
                padding: EdgeInsets.fromLTRB(8.0, 32.0, 8.0, 32.0),
                itemCount: 8,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
                    margin: EdgeInsets.all(8),
                    child: InkWell(
                      onTap: () {},
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 280,
                            width: double.infinity,
                            color: Colors.black26,
                            child: Center(
                              child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.black26),
                              ),
                            ),
                          ),
                          ListTile(
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(4.0),
                                  child: Container(
                                    color: Colors.black26,
                                    width: 80,
                                    height: 24,
                                  ),
                                ),
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(4.0),
                                  child: Container(
                                    color: Colors.black26,
                                    width: 80,
                                    height: 24,
                                  ),
                                ),
                              ],
                            ),
                            trailing: ClipRRect(
                              borderRadius: BorderRadius.circular(4.0),
                              child: Container(
                                color: Colors.black26,
                                width: 40,
                                height: 24,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
                });
          case ConnectionState.done:
            if (snapshot.hasError) return Text('Error: ${snapshot.error}');
            return ListView.builder(
                padding: EdgeInsets.fromLTRB(8.0, 32.0, 8.0, 32.0),
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return Card(
//                    shape: RoundedRectangleBorder(
//                        borderRadius: BorderRadius.all(Radius.circular(8))),
                    margin: EdgeInsets.all(8),
                    child: InkWell(
                      onTap: () {
//                          Navigator.push(
//                            context,
//                            MaterialPageRoute(builder: (context) => FindLost()),
//                          );

                        _openDialog(snapshot.data[index]);
                      },
                      child: Column(
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.only(
                                topRight: Radius.circular(4),
                                topLeft: Radius.circular(4)),
                            child: CachedNetworkImage(
                              imageUrl: snapshot.data[index].album_file,
                              fit: BoxFit.cover,
                              height: 280,
                              width: double.infinity,
                              placeholder: (context, url) => Container(
                                height: 280,
                                width: double.infinity,
                                color: Colors.black26,
                                child: Center(
                                    child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.black26),
                                )),
                              ),
                              errorWidget: (context, url, error) => Container(
                                color: Colors.black54,
                                height: 140,
                                width: double.infinity,
                                child: Icon(
                                  Icons.pets,
                                  size: 60,
                                  color: Colors.black38,
                                ),
                              ),
                            ),
                          ),
                          ExpansionTile(
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Padding(
                                  child: Text(
                                    animalModel.getLocale(
                                        snapshot.data[index].animal_area_pkid),
                                    style: TextStyle(
                                        color: Colors.black54, fontSize: 18),
                                  ),
                                  padding: EdgeInsets.only(left: 8),
                                ),
                                Row(
                                  children: <Widget>[
                                    IconButton(
                                      icon: Icon(Icons.share,
                                          color: Colors.black45),
                                      onPressed: () {
                                        Share.share(snapshot
                                                .data[index].shelter_name +
                                            ' ' +
                                            snapshot.data[index].album_file);
                                      },
                                    ),
                                    IconButton(
                                      icon: getFavoriteIcon(
                                          snapshot.data[index].animal_id),
                                      onPressed: () {
                                        saveFavorite(
                                            snapshot.data[index].animal_id);
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            children: <Widget>[
                              ListTile(
                                leading: Icon(
                                  Icons.local_offer,
                                  color: primaryColor,
                                ),
                                title: Container(
                                  child: Wrap(
                                    children: <Widget>[
                                      getChip(animalModel.getKind(
                                          snapshot.data[index].animal_kind)),
                                      getChip(animalModel.getSex(
                                          snapshot.data[index].animal_sex)),
                                      getChip(animalModel.getBody(snapshot
                                          .data[index].animal_bodytype)),
                                      getChip(animalModel.getColour(
                                          snapshot.data[index].animal_colour)),
                                      getChip(animalModel.getAge(
                                          snapshot.data[index].animal_age)),
                                    ],
                                  ),
                                ),
                              ),
                              ListTile(
                                leading: Icon(
                                  Icons.place,
                                  color: primaryColor,
                                ),
                                subtitle: Text(animalModel.getCurrentPlace(
                                    snapshot.data[index].animal_place)),
                                title: Text(
                                  '所在地',
                                ),
                              ),
                              ListTile(
                                leading: Icon(
                                  Icons.update,
                                  color: primaryColor,
                                ),
                                subtitle: Text(snapshot.data[index].cDate),
                                title: Text('資料更新時間'),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                });
        }
        return null;
      },
    );
  }
}
