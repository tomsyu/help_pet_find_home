import 'package:flutter/material.dart';

const primaryColor = const Color(0xffe57373);
const primaryColorLight = const Color(0xffffa4a2);
const primaryColorDark = const Color(0xffaf4448);
const primaryColorCheck = const Color(0xffba6b6c);
