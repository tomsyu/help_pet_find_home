import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:help_pet_find_home/Adoption.dart';
import 'package:help_pet_find_home/Desgin/color.dart';
import 'package:help_pet_find_home/animal_datas.dart';
import 'package:help_pet_find_home/animal_model.dart';
import 'package:help_pet_find_home/model/app_state.dart';
import 'package:help_pet_find_home/model/filter.dart';
import 'package:help_pet_find_home/model/filter_layer.dart';
import 'package:help_pet_find_home/my_favorite_page.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'model/filter_list.dart';

// TODO: Add velocity constant (104)
const double _kFlingVelocity = 2.0;

class Backdrop extends StatefulWidget {
  @override
  _BackdropState createState() => _BackdropState();
}

class _BackdropState extends State<Backdrop>
    with SingleTickerProviderStateMixin {
  final GlobalKey _backdropKey = GlobalKey(debugLabel: 'Backdrop');
  // TODO: Add AnimationController widget (104)

  // TODO: Add BuildContext and BoxConstraints parameters to _buildStack (104)
  AnimationController _controller;
  TabController tabController;

  String appTitle = '搜尋中..';
  Filter filter;
  AnimalModel animalModel = AnimalModel();
  Future<List<AnimalData>> futureDatas;
  Future<List<AnimalData>> futureAdoptionDatas;
  Future<List<AnimalData>> futureFavoriteDatas;
  SharedPreferences prefs;
  List<String> favoriteList;
  int _backDropBtnIndex;
  String _backDropBtnText;

  AnimalDatas animalData;

  Future<String> _onCategoryTap(FilterList filterList) {
    filter.animal_sex = filterList.animal_sex;
    filter.animal_bodytypes = filterList.animal_bodytype;
    filter.animal_kind = filterList.animal_kind;
    filter.animal_sterilization = filterList.animal_sterilization;
    filter.animal_age = filterList.animal_age;
    filter.animal_area_pkid = filterList.animal_area_pkid;
    filter.animal_shelter_pkid = filterList.animal_shelter_pkid;

    futureAdoptionDatas = filter.startFutureFilter();
    initAppTitle();
  }

  Future<List<AnimalData>> callApi() async {
    String url =
        'https://data.coa.gov.tw/Service/OpenData/TransService.aspx?UnitId=QcbUEzN6E6DL&album_file=http%25';
    Response response = await get(url);
    final responseJson = await json.decode(response.body);
    animalData = AnimalDatas.fromJson(responseJson);
    filter = Filter(animalData.animalDatas);

    futureFavoriteDatas = getFavorite();

//    animalData.animalDatas=filter.startFutureFilter();
    return animalData.animalDatas;
  }

  Future<List<AnimalData>> getFavorite() async {
    List<String> stringList = List<String>();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getStringList('Favorite_list') == null) {
      prefs.setStringList('Favorite_list', List<String>());
      stringList = prefs.getStringList('Favorite_list');
    } else {
      stringList = prefs.getStringList('Favorite_list');
    }
    List<int> favoriteList = stringList.map(int.parse).toList();

    filter.animal_id = favoriteList;
    return filter.startFavoriteFutureFilter();
  }

  bool get _frontLayerVisible {
    final AnimationStatus status = _controller.status;
    return status == AnimationStatus.completed ||
        status == AnimationStatus.forward;
  }

  void _toggleBackdropLayerVisibility() {
    _controller.fling(velocity: _frontLayerVisible ? -10 : _kFlingVelocity);
    _backDropBtnIndex = _frontLayerVisible ? 0 : 1;
  }

  void _toggleLayerVisibilityAndOnCategoryTap() {
    _onCategoryTap(Provider.of<AppState>(context, listen: false).filterList);
    _toggleBackdropLayerVisibility();

//    Provider.of<AppState>(context, listen: false).update();
  }

  void getMyFavoritePage() {
    futureDatas.then((s) {
      setState(() {
        filter = Filter(animalData.animalDatas);
      });
    });
  }

  void initAppTitle() {
    futureAdoptionDatas.then((s) {
      setState(() {
        String animalCount = s.length.toString();
        appTitle = "共" + animalCount + "個毛小孩";
        _backDropBtnText = animalCount;
      });
    });
  }

  Future<void> initSharePreferences() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.getStringList('Favorite_list') == null) {
      prefs.setStringList('Favorite_list', List<String>());
      favoriteList = prefs.getStringList('Favorite_list');
    } else {
      favoriteList = prefs.getStringList('Favorite_list');
    }
  }

  @override
  void initState() {
    _controller = AnimationController(
      duration: Duration(milliseconds: 300),
      value: 1.0,
      vsync: this,
    );
    _controller.addStatusListener((status) {
      if (status == AnimationStatus.reverse) {
        print('completed');
      }
    });

    initSharePreferences();
    futureDatas = callApi();
    futureAdoptionDatas = futureDatas;
//    futureAdoptionDatas = getFavorite();
    initAppTitle();
    _backDropBtnIndex = 0;
    _backDropBtnText = ' ';

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  //建構寵物主畫面
  Widget _buildStack(BuildContext context, BoxConstraints constraints) {
    const double layerTitleHeight = 48; //48
    final Size layerSize = constraints.biggest;
    final double layerTop = layerSize.height - layerTitleHeight;

    // TODO: Create a RelativeRectTween Animation (104)
    Animation<RelativeRect> layerAnimation = RelativeRectTween(
      begin: RelativeRect.fromLTRB(0.0, layerTop, 0.0, 0.0),
      end: RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0),
    ).animate(_controller.view);

    return Consumer<AppState>(
      builder: (context, appState, child) {
        Widget getActionChip() {
          if (appState.isFilterListHasChild() &&
              appState.isFilterStateHasChild()) {
            return Padding(
              padding: EdgeInsets.only(right: 8),
              child: ActionChip(
                label: Text(
                  '清除篩選',
                  style: TextStyle(color: Colors.white),
                ),
                avatar: Icon(
                  Icons.clear_all,
                  color: Colors.white,
                ),
                onPressed: () {
                  appState.clearAll();
                  futureAdoptionDatas = filter.startFutureFilter();
                  initAppTitle();
                },
                backgroundColor: Colors.black38,
//                shape: StadiumBorder(
//                  side: BorderSide(color: primaryColorLight),
//                ),
              ),
            );
          } else {
            return SizedBox.shrink();
          }
        }

        return Stack(
          key: _backdropKey,
          children: <Widget>[
            ExcludeSemantics(
              child: FilterLayer(
                onCategoryTap: _onCategoryTap,
                onSearchPress: _toggleBackdropLayerVisibility,
              ),
              excluding: _frontLayerVisible,
            ),
            // TODO: Add a PositionedTransition (104)
            PositionedTransition(
              rect: layerAnimation,
              child: _FrontLayer(
                // TODO: Implement onTap property on _BackdropState (104)
                child: DefaultTabController(
                  length: 3,
                  child: Column(
                    children: <Widget>[
                      Material(
                        elevation: 4,
                        color: primaryColorLight,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16.0),
                              topRight: Radius.circular(16.0)),
                        ),
                        child: IndexedStack(
                          index: _backDropBtnIndex,
                          children: <Widget>[
                            Container(
                              child: TabBar(
                                tabs: [
                                  Tab(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(Icons.pets),
                                        Text(' 首頁 ')
                                      ],
                                    ),
                                  ),
                                  Tab(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(Icons.favorite),
                                        Text(' 最愛 ')
                                      ],
                                    ),
                                  ),
                                  Tab(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(Icons.search),
                                        Text(' 尋找遺失 ')
                                      ],
                                    ),
                                  ),
                                ],
                                indicatorColor: Colors.white,
                              ),
                            ),
                            GestureDetector(
                              child: Container(
                                height: 48,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 16),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        _backDropBtnText + '個毛小孩',
                                        style: TextStyle(
                                            fontSize: 16, color: Colors.white),
                                      ),
                                      Icon(
                                        Icons.keyboard_arrow_up,
                                        color: Colors.white,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              onTap: () {
                                _toggleLayerVisibilityAndOnCategoryTap();
                              },
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Container(
                          child: TabBarView(children: [
                            Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 8,
                                ),
                                Wrap(
                                  children: <Widget>[
                                    getActionChip(),
                                    Wrap(
                                        spacing: 8,
                                        runSpacing: 8,
                                        children: appState.filterList.animal_sex
                                            .map<Widget>((s) {
                                          return HomeChip(
                                            label: animalModel.getSex(s),
                                            onDeleted: () {
                                              setState(() {
                                                appState.filterState
                                                    .animal_sex[s] = false;
                                                appState.filterList.animal_sex
                                                    .remove(s); // 刪除事件
                                                futureAdoptionDatas =
                                                    filter.startFutureFilter();
                                                initAppTitle();
                                              });
                                            },
                                          );
                                        }).toList()),
                                    Wrap(
                                        spacing: 8,
                                        runSpacing: 5,
                                        children: Provider.of<AppState>(context,
                                                listen: false)
                                            .filterList
                                            .animal_kind
                                            .map<Widget>((s) {
                                          return HomeChip(
                                            label: animalModel.getKind(s),
                                            onDeleted: () {
                                              setState(() {
                                                appState.filterState
                                                    .animal_kind[s] = false;
                                                appState.filterList.animal_kind
                                                    .remove(s); // 刪除事件
                                                futureAdoptionDatas =
                                                    filter.startFutureFilter();
                                                initAppTitle();
                                              });
                                            },
                                          );
                                        }).toList()),
                                    Wrap(
                                      spacing: 8,
                                      runSpacing: 8,
                                      children: appState
                                          .filterList.animal_bodytype
                                          .map<Widget>((s) {
                                        return HomeChip(
                                          label: animalModel.getBody(s),
                                          onDeleted: () {
                                            setState(() {
                                              appState.filterState
                                                  .animal_bodytype[s] = false;
                                              appState
                                                  .filterList.animal_bodytype
                                                  .remove(s); // 刪除事件
                                              futureAdoptionDatas =
                                                  filter.startFutureFilter();
                                              initAppTitle();
                                            });
                                          },
                                        );
                                      }).toList(),
                                    ),
                                    Wrap(
                                      spacing: 8,
                                      runSpacing: 8,
                                      children: appState.filterList.animal_age
                                          .map<Widget>((s) {
                                        return HomeChip(
                                          label: animalModel.getAge(s),
                                          onDeleted: () {
                                            setState(() {
                                              appState.filterState
                                                  .animal_age[s] = false;
                                              appState.filterList.animal_age
                                                  .remove(s); // 刪除事件
                                              futureAdoptionDatas =
                                                  filter.startFutureFilter();
                                              initAppTitle();
                                            });
                                          },
                                        );
                                      }).toList(),
                                    ),
                                    Wrap(
                                      spacing: 8,
                                      runSpacing: 8,
                                      children: appState
                                          .filterList.animal_sterilization
                                          .map<Widget>((s) {
                                        return HomeChip(
                                          label:
                                              animalModel.getSterilization(s),
                                          onDeleted: () {
                                            setState(() {
                                              appState.filterState
                                                      .animal_sterilization[s] =
                                                  false;
                                              appState.filterList
                                                  .animal_sterilization
                                                  .remove(s); // 刪除事件
                                              futureAdoptionDatas =
                                                  filter.startFutureFilter();
                                              initAppTitle();
                                            });
                                          },
                                        );
                                      }).toList(),
                                    ),
                                    Wrap(
                                      spacing: 8,
                                      runSpacing: 8,
                                      children: appState
                                          .filterList.animal_area_pkid
                                          .map<Widget>((s) {
                                        return HomeChip(
                                          label: animalModel.getLocale(s),
                                          onDeleted: () {
                                            setState(() {
                                              appState.filterState
                                                  .animal_area_pkid[s] = false;
                                              appState
                                                  .filterList.animal_area_pkid
                                                  .remove(s); // 刪除事件
                                              futureAdoptionDatas =
                                                  filter.startFutureFilter();
                                              initAppTitle();
                                            });
                                          },
                                        );
                                      }).toList(),
                                    ),
                                    Wrap(
                                      spacing: 8,
                                      runSpacing: 8,
                                      children: appState
                                          .filterList.animal_shelter_pkid
                                          .map<Widget>((s) {
                                        return HomeChip(
                                          label: animalModel
                                              .getShelterNameByPkID(s),
                                          onDeleted: () {
                                            setState(() {
                                              appState.filterState
                                                  .animal_shelter_pkid[0] = -1;
                                              appState.filterList
                                                  .animal_shelter_pkid
                                                  .remove(s); // 刪除事件
                                              futureAdoptionDatas =
                                                  filter.startFutureFilter();
                                              initAppTitle();
                                            });
                                          },
                                        );
                                      }).toList(),
                                    ),
                                  ],
                                ),
                                Expanded(child: Adoption(futureAdoptionDatas))
                              ],
                            ),
                            MyFavoritePage(futureFavoriteDatas, filter),
                            Container(
                              color: primaryColor,
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Image.asset(
                                      'images/franch_dog.png',
                                      width: 100,
                                      height: 200,
                                    ),
                                    Text(
                                      '開發中.',
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 22),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ]),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final _scaffoldKey = GlobalKey<ScaffoldState>();
    var appBar = AppBar(
      elevation: 0.0,
      titleSpacing: 0.0,
      // TODO: Replace leading menu icon with IconButton (104)
      // TODO: Remove leading property (104)
      // TODO: Create title with _BackdropTitle parameter (104)
      leading: IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.close_menu,
          progress: _controller,
        ),
        onPressed: _toggleLayerVisibilityAndOnCategoryTap,
      ),
      title: Text(
        appTitle,
        style: TextStyle(fontSize: 20),
      ),
      actions: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Center(
            child: FlatButton(
              child: Text(
                '重設篩選',
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                if (Provider.of<AppState>(context, listen: false)
                        .isFilterListHasChild() &&
                    Provider.of<AppState>(context, listen: false)
                        .isFilterStateHasChild()) {
                  Provider.of<AppState>(context, listen: false).clearAll();
                  futureAdoptionDatas = filter.startFutureFilter();
                  initAppTitle();

//                _scaffoldKey.currentState
//                    .showSnackBar(SnackBar(content: Text("Welcome")));
                }
              },
            ),
          ),
        )
        // TODO: Add shortcut to login screen from trailing icons (104)
      ],
    );
    return Scaffold(
      key: _scaffoldKey,
      appBar: appBar,
      // TODO: Return a LayoutBuilder widget (104)
      body: LayoutBuilder(builder: _buildStack),
    );
  }
}

class _FrontLayer extends StatelessWidget {
  // TODO: Add on-tap callback (104)
  const _FrontLayer({
    Key key,
    this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 16.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          // TODO: Add a GestureDetector (104)
          Expanded(
            child: child,
          ),
        ],
      ),
    );
  }
}

class HomeChip extends StatelessWidget {
  String label;
  VoidCallback onDeleted;

  HomeChip({this.label, this.onDeleted});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8),
      child: Chip(
        label: Text(
          label,
          style: TextStyle(color: Colors.black54),
        ),
        backgroundColor: Colors.transparent,
        shape: StadiumBorder(
          side: BorderSide(color: Colors.black12),
        ),
        onDeleted: onDeleted,
        deleteIconColor: Colors.black45,
      ),
    );
  }
}
