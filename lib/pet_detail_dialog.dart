import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:help_pet_find_home/Desgin/color.dart';
import 'package:help_pet_find_home/animal_datas.dart';
import 'package:help_pet_find_home/animal_model.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class PetIntroDialog extends StatefulWidget {
  AnimalData animalData;

  PetIntroDialog(this.animalData);

  @override
  _PetIntroDialogState createState() => _PetIntroDialogState();
}

class _PetIntroDialogState extends State<PetIntroDialog> {
  AnimalModel animalModel = AnimalModel();
  SharedPreferences prefs;
  List<String> favoriteList;

  Widget getChip(String result) {
    if (result != null) {
      return Padding(
        padding: EdgeInsets.only(left: 8),
        child: Chip(
          labelStyle: TextStyle(color: Colors.black54),
          label: Text(result),
        ),
      );
    } else {
      return Container();
    }
  }

  Widget getFavoriteIcon(int animalId) {
    if (favoriteList == null) {
      return Icon(
        Icons.favorite_border,
        color: Colors.white,
      );
    }
    if (favoriteList.contains(animalId.toString())) {
      return Icon(
        Icons.favorite,
        color: Colors.white,
      );
    } else {
      return Icon(
        Icons.favorite_border,
        color: Colors.white,
      );
    }
  }

  Future<void> initSharePreferences() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      if (prefs.getStringList('Favorite_list') == null) {
        prefs.setStringList('Favorite_list', List<String>());
        favoriteList = prefs.getStringList('Favorite_list');
      } else {
        favoriteList = prefs.getStringList('Favorite_list');
      }
    });
  }

  Widget getRemarkListTile() {
    if (widget.animalData.animal_remark == '') {
      return Container();
    } else {
      return ListTile(
        dense: true,
        leading: Icon(
          Icons.rate_review,
          color: primaryColor,
        ),
        subtitle: Text(animalModel.getRemark(widget.animalData.animal_remark)),
        title: Text(
          '備註',
          style: TextStyle(fontSize: 15),
        ),
      );
    }
  }

  saveFavorite(int animalId) {
    setState(() {
      bool isContainID = favoriteList.contains(animalId.toString());
      if (!isContainID) {
        favoriteList.add(animalId.toString());
        prefs.setStringList('Favorite_list', favoriteList);
      } else {
        favoriteList.remove(animalId.toString());
        prefs.setStringList('Favorite_list', favoriteList);
      }
    });
  }

  _launchCaller(String tel) async {
    String url = "tel:$tel";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    initSharePreferences();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('關於我'),
        actions: [
          IconButton(
            icon: getFavoriteIcon(widget.animalData.animal_id),
            color: Colors.white,
            onPressed: () {
              saveFavorite(widget.animalData.animal_id);
            },
          ),
          IconButton(
            icon: Icon(Icons.share),
            color: Colors.white,
            onPressed: () {
              Share.share(widget.animalData.shelter_name +
                  ' ' +
                  widget.animalData.album_file);
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          child: Column(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(4), topLeft: Radius.circular(4)),
                child: CachedNetworkImage(
                  imageUrl: widget.animalData.album_file,
                  fit: BoxFit.fitWidth,
                  width: double.infinity,
                  placeholder: (context, url) => Container(
                    child: CircularProgressIndicator(),
                  ),
                  errorWidget: (context, url, error) => Container(
                    color: Colors.black54,
                    height: 160,
                    width: double.infinity,
                    child: Icon(
                      Icons.pets,
                      size: 60,
                      color: Colors.black38,
                    ),
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                height: 48,
                child: RaisedButton.icon(
                  color: primaryColor,
                  icon: Icon(
                    Icons.call,
                    color: Colors.white,
                  ),
                  label: Text(
                    '聯絡收容中心',
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                  onPressed: () {
                    _launchCaller(widget.animalData.shelter_tel);
                  },
                ),
              ),
              Divider(),
              ListTile(
                leading: Icon(
                  Icons.place,
                  color: primaryColor,
                  size: 36,
                ),
                subtitle: Text(animalModel
                    .getCurrentPlace(widget.animalData.animal_place)),
                title: Text(
                  '所在地',
                  style: TextStyle(fontSize: 15),
                ),
                onTap: () {
                  MapsLauncher.launchQuery(widget.animalData.shelter_address);
                },
              ),
              Divider(),
              ListTile(
                leading: Icon(
                  Icons.local_offer,
                  color: primaryColor,
                ),
                title: Container(
                  child: Wrap(
                    children: <Widget>[
                      getChip(
                          animalModel.getKind(widget.animalData.animal_kind)),
                      getChip(animalModel.getSex(widget.animalData.animal_sex)),
                      getChip(animalModel
                          .getBody(widget.animalData.animal_bodytype)),
                      getChip(animalModel
                          .getColour(widget.animalData.animal_colour)),
                      getChip(animalModel.getAge(widget.animalData.animal_age)),
                      getChip(animalModel.getSterilization(
                          widget.animalData.animal_sterilization)),
                    ],
                  ),
                ),
              ),
              getRemarkListTile(),
              ListTile(
                leading: Icon(
                  Icons.not_listed_location,
                  color: primaryColor,
                ),
                subtitle: Text(animalModel
                    .getFoundPlace(widget.animalData.animal_foundplace)),
                title: Text(
                  '尋獲地',
                  style: TextStyle(fontSize: 15),
                ),
              ),
              Divider(),
              DetailInfoTile(
                  title: '資料建立時間:', text: widget.animalData.animal_createtime),
              DetailInfoTile(
                  title: '最後更新時間:', text: widget.animalData.animal_update),
              DetailInfoTile(
                  title: '開放認養時間:', text: widget.animalData.animal_opendate),
              DetailInfoTile(
                  title: '區域編號:', text: widget.animalData.animal_id.toString()),
              DetailInfoTile(
                  title: '流水編號:',
                  text: widget.animalData.animal_subid.toString()),
              SizedBox(
                height: 16,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class DetailInfoTile extends StatelessWidget {
  String title;
  String text;
  DetailInfoTile({this.title, this.text});
  TextStyle style = TextStyle(color: Colors.black54, fontSize: 11);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        SizedBox(width: 24),
        Text(title, style: style),
        Text(
          text,
          style: style,
        )
      ],
    );
  }
}
